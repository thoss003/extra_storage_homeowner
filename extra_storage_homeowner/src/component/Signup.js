import React, { Component } from 'react';
import * as firebase from "firebase";

import {
    StyleSheet,
    Text,
    TextInput,
    ScrollView,
    View,
    Button,
    TouchableOpacity,
    AsyncStorage,
    Image
    
} from 'react-native';
import login from './Login';


export default class Signup extends React.Component {
    state = {
        // name: '', password: '', email: '', phone_number: ''
        name: "",
        email: "",
        password: "",
        phone_number: "",
        errorMessage: null
    }
    // onChangeText = (key, val) => {
    //   this.setState({ [key]: val })
    // }
    // signup = async () => {
    //   const { name, password,postcode, email, phone_number } = this.state
    //   try {
    //     // here place your signup logic
    //     console.log('user successfully signed up!: ', success)
    //   } catch (err) {
    //     console.log('error signing up: ', err)
    //   }
    // }
    handleSignUp = () => {
      firebase
          .auth()
          .createUserWithEmailAndPassword(this.state.email, this.state.password)
          .then(() => this.props.navigation.navigate('Login'))
          // .then(userCredentials => {
          //     return userCredentials.user.updateProfile({
          //       displayName: this.state.name
          //     });
          // })
          .catch(error => this.setState({ errorMessage: error.message }));
  };
   
    render() {
        const { navigate } = this.props.navigation;
      return (
        <View style={styles.container}>

          <Text
            style={{ color:'white',
                     alignItems: 'center',
                     fontSize: 30}}>Signup</Text>
          <TextInput
            style={styles.input}
            placeholder='Name'
            autoCapitalize="none"
            placeholderTextColor='white'
            onChangeText={name => this.setState({ name })}
            value={this.state.name}
          />
          <TextInput
            style={styles.input}
            placeholder='Password'
            secureTextEntry={true}
            autoCapitalize="none"
            placeholderTextColor='white'
            onChangeText={password => this.setState({ password })}
            value={this.state.password}
          />

          <TextInput
            style={styles.input}
            placeholder='Email'
            autoCapitalize="none"
            placeholderTextColor='white'
            onChangeText={email => this.setState({ email })}
            value={this.state.email}
          />

          <TextInput
            style={styles.input}
            placeholder='Phone Number'
            autoCapitalize="none"
            placeholderTextColor='white'
            onChangeText={phone_number => this.setState({ phone_number })}
            value={this.state.phone_number}
          />
            
          <TouchableOpacity 
              style={styles.userBtn}
              onPress={this.handleSignUp}>
              <Text style={styles.btnTxt}>Sign Up</Text>
          </TouchableOpacity>
            

            <View>
                    <TouchableOpacity
                    onPress={() => navigate('Login')}>
                        <Text></Text>
                        <Text style={{color: '#fff',padding:10}}>Already a member? Login</Text>
                    </TouchableOpacity>
            </View>
        </View>
      )
    }
  }
  
  const styles = StyleSheet.create({
    input: {
      borderColor: 'white',
      borderWidth: 1,
      width: '95%',
      height: 50,
      margin: 10,
      padding: 10,
      color: 'white',
      fontSize: 17,
      fontWeight: '300',
    },
    container: {
      backgroundColor :'#121212',
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    btnTxt: {
        fontSize: 18,
        textAlign:"center"
    },
    userBtn: {
        backgroundColor: "#fff",
        padding:15,
        width:"95%",
        borderRadius:10,
    },
    error: {
      color: "#E9446A",
      fontSize: 13,
      fontWeight: "600",
      textAlign: "center"
    },
    errorMessage: {
      height: 72,
      alignItems: "center",
      justifyContent: "center",
      marginHorizontal: 30
    }
  })