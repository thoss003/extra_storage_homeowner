import React, { Component } from 'react';
import { Icon } from 'react-native-elements';



import {
  StyleSheet,
  Text,
  TextInput,
  ScrollView,
  View,
  Button,
  TouchableOpacity,
  TouchableHighlight,
  AsyncStorage,
  Dimensions,
  Image

}
  from 'react-native';

import * as firebase from "firebase";

export default class Homepage extends React.Component {
  static navigationOptions = {
    headerShown: false,
  }

  state = { currentUser: null }

  componentDidMount() {
    const { currentUser } = firebase.auth()

    this.setState({ currentUser })
  }

  render() {

    const { currentUser } = this.state
    const { navigate } = this.props.navigation;
    return (

      <View style={styles.container}>

        <View style={styles.top}>
          {/* <Text style={{color:"red"}}> Hi {currentUser && currentUser.email}! </Text> */}
          <Image
            style={{ width: 250, height: 250 }}
            source={require('../../assets/ES_logo.png')} />
        </View>

        <View style={styles.center}>



          {/* 1 */}
          <View style={styles.centerItem}>
            <View style={styles.centerItemInner}>
              <TouchableOpacity onPress={() => navigate('ProductsOverview')}>
                <Image style={styles.itemIcon}
                  source={require('../../assets/icon/add-space.png')} />
                <Text textAlign="center" >ADD SPACE</Text>
              </TouchableOpacity>
            </View>
          </View>

          {/* 2 */}

          <View style={styles.centerItem}>
            <View style={styles.centerItemInner}>
              <TouchableOpacity onPress={() => navigate('Map')}>
                <Image style={styles.itemIcon}
                  source={require('../../assets/icon/map.png')} />
                <Text textAlign="center" >MAP</Text>
              </TouchableOpacity>
            </View>
          </View>

          {/* 3 */}
          <View style={styles.centerItem}>
            <View style={styles.centerItemInner}>
              <TouchableOpacity onPress={() => navigate('Chat')}>
                <Image style={styles.itemIcon}
                  source={require('../../assets/icon/chat.png')} />
                <Text textAlign="center" >CHAT</Text>
              </TouchableOpacity>
            </View>
          </View>


          {/* 4 */}
          <View style={styles.centerItem}>
            <View style={styles.centerItemInner}>
              <TouchableOpacity onPress={() => navigate('Stored')}>
                <Image style={styles.itemIcon}
                  source={require('../../assets/icon/024-moving-4.png')} />
                <Text textAlign="center" >STORED NOW</Text>
              </TouchableOpacity>
            </View>
          </View>

          {/* 5 */}
          <View style={styles.centerItem}>
            <View style={styles.centerItemInner}>
              <TouchableOpacity onPress={() => navigate('Setting')}>
                <Image style={styles.itemIcon}
                  source={require('../../assets/icon/settings.png')} />
                <Text textAlign="center" >SETTING</Text>
              </TouchableOpacity>
            </View>
          </View>

          {/* 6 */}
          <View style={styles.centerItem}>
            <View style={styles.centerItemInner}>
              <TouchableOpacity onPress={() => navigate('Review')}>
                <Image style={styles.itemIcon}
                  source={require('../../assets/icon/review.png')} />
                <Text textAlign="center" >REVIEW</Text>
              </TouchableOpacity>
            </View>
          </View>

          {/* 7 */}
          <View style={styles.centerItem}>
            <View style={styles.centerItemInner}>
              <TouchableOpacity onPress={() => navigate('Profile')}>
                <Image style={styles.itemIcon}
                  source={require('../../assets/icon/my-profile.png')} />
                <Text textAlign="center" >MY PROFILE</Text>
              </TouchableOpacity>
            </View>
          </View>

          {/* 8 */}
          <View style={styles.centerItem}>
            <View style={styles.centerItemInner}>
              <TouchableOpacity onPress={() => navigate('Payment')}>
                <Image style={styles.itemIcon}
                  source={require('../../assets/icon/payment.png')} />
                <Text textAlign="center" >PAYMENTS</Text>
              </TouchableOpacity>
            </View>
          </View>

          {/* 9 */}
          <View style={styles.centerItem}>
            <View style={styles.centerItemInner}>
              <TouchableOpacity onPress={() => navigate('Support')}>
                <Image style={styles.itemIcon}
                  source={require('../../assets/icon/support.png')} />
                <Text>SUPPORT</Text>
              </TouchableOpacity>
            </View>
          </View>

        </View>

        <View style={styles.bottom}>
          <TouchableOpacity style={styles.userBtn}
            onPress={() => navigate('Login')}>
            <Text style={styles.btnTxt}>Logout</Text>
          </TouchableOpacity>
          <Text style={styles.itemTitle}>Title</Text>
        </View>

      </View>

    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent:'center',
    //  alignItems:'center',
    backgroundColor: 'black',
    // width:'100%'
  },
  top: {
    width: '100%',
    height: '35%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black'
  },
  center: {
    //height:'50%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginBottom: 110,
    justifyContent: "space-between",
    //backgroundColor: '#fff',
    shadowColor: 'white',
    shadowOpacity: 6.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    //elevation: 150,
    borderRadius: 10,
    backgroundColor: 'white',
    height: 340,
    margin: 20
  },
  centerItem: {
    // height:'30%',
    // width:'30%',
    // padding:5,
    // alignItems: 'center',
    // justifyContent: 'center'
    width: Dimensions.get('window').width * 0.30,
    height: 100,
    //borderWidth: 1,
    //borderColor: "lightgray",
    alignItems: 'center',
    justifyContent: 'center'

  },
  centerItemInner: {
    // flex:1,
    // backgroundColor:'white',
    // resizeMode: 'contain'
    margin: 10,
    width: '80%',
    height: 50,
    resizeMode: 'contain',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  bottom: {
    //height:'10%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  userBtn: {
    backgroundColor: "red",
    padding: 15,
    width: "100%",
    borderRadius: 10,
  },
  btnTxt: {
    fontSize: 20,
    textAlign: "center",
    backgroundColor: 'red'
  },
  itemIcon: {
    // margin: 10,
    //justifyContent:'center',
    //alignItems:'center',
    width: 80,
    height: 80,
    resizeMode: 'contain'
  },
  itemTitle: {
    marginTop: 16,
    textDecorationColor: 'green'
  },
  Box: {
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 5,
    borderRadius: 10,
    backgroundColor: 'white',
    height: 300,
    margin: 20
  }
})