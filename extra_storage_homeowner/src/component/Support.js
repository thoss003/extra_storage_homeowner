import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  TextInput,
  ScrollView,
  View,
  Button,
  TouchableOpacity,
  AsyncStorage,
  Image

} from 'react-native';

import * as firebase from "firebase";

export default class Support extends Component {

  state = { currentUser: null }

  componentDidMount() {
    const { currentUser } = firebase.auth()

    this.setState({ currentUser })
  }

  render() {
    const { currentUser } = this.state
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.container}>
        <View>
          <Image style={styles.itemIcon}
            source={require('../../assets/screens/Support.png')} />
        </View>

      </View>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})