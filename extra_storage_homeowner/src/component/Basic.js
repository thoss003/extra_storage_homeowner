import React, { Component } from 'react';

import {
    StyleSheet,
    Text,
    TextInput,
    ScrollView,
    View,
    Button,
    TouchableOpacity,
    AsyncStorage,
    Image
    
} from 'react-native';


// export default class Homepage extends Component {

   
//     render() {
//         const { navigate } = this.props.navigation;
//       return (
//         <View>
//           <TouchableOpacity onPress={() => navigate('Login')}>
//             <Text >Logout</Text>
//           </TouchableOpacity>
//         </View>
//       )
//     }
//   }


// import firebase from 'react-native-firebase'

import * as firebase from "firebase";

export default class Basic extends Component {

  state = { currentUser: null }

  componentDidMount() {
    const { currentUser } = firebase.auth()

    this.setState({ currentUser })
  }

  render() {
    const { currentUser } = this.state
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.container}>
        <Text>
          Hi {currentUser && currentUser.email}!
        </Text>
        <View>
        <TouchableOpacity 
                       
                       onPress={() => navigate('Login')}
                        >
                        <Text>Logout</Text>
                    </TouchableOpacity>
        </View>
        {/* <View>
          <Icon
            raised
            name='heartbeat'
            type='font-awesome'
            color='#f50'
            onPress={() => console.log('hello')} />
          <Icon
            raised
            name='search'
            type='font-awesome'
            color='#f50'
            onPress={() => console.log('hello')} />
        </View> */}
      </View>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})