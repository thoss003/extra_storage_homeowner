import React, { useState } from 'react';
import { createStore, combineReducers,applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import * as firebase from "firebase";
import { AppLoading } from 'expo';
import * as Font from 'expo-font';
import { composeWithDevTools } from 'redux-devtools-extension';
import ReduxThunk from 'redux-thunk';


import productsReducer from './store/reducers/products';
import ShopNavigator from './navigation/ShopNavigator';
import Color from './constants/Colors';
import cartReducer from './store/reducers/cart';
import cart from './store/reducers/cart';
import ordersReducer from './store/reducers/orders';


// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyBowBtHh6q44iHaNJiEQxIxzaAPEc_iiBE",
  authDomain: "extra-storage-c48c0.firebaseapp.com",
  databaseURL: "https://extra-storage-c48c0.firebaseio.com",
  projectId: "extra-storage-c48c0",
  storageBucket: "extra-storage-c48c0.appspot.com",
  messagingSenderId: "539210301030",
  appId: "1:539210301030:web:44b349f26ed7b3fed677a7",
  measurementId: "G-7VV49Y7Z96"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
//firebase.analytics();

const fetchFonts = () => {
  return Font.loadAsync({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
  });
};

const rootReducer = combineReducers({
  products: productsReducer,
  cart: cartReducer,
  orders: ordersReducer
});

const store = createStore(rootReducer,applyMiddleware(ReduxThunk), composeWithDevTools ());


export default function App() {
  const [fontLoaded, setFontLoaded] = useState(false);

  if (!fontLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => {
          setFontLoaded(true);
        }}
      />
    );
  }
  return (
    <Provider store={store}>
      <ShopNavigator />
    </Provider>
  );
}