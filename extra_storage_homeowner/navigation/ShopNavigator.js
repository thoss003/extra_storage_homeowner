
import React from 'react';

import {
  createAppContainer,
  createSwitchNavigator
} from 'react-navigation';

import { createDrawerNavigator } from 'react-navigation-drawer';
import { Ionicons } from '@expo/vector-icons';
import { StyleSheet, Text, View, Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';

// import { createBottomTabNavigator } from "react-navigation-tabs";

import Homepage from '../src/component/HomePage';
import Chat from '../src/component/Chat';
import Map from '../src/component/Map';
import Payment from '../src/component/Payment';
import Profile from '../src/component/Profile';
import Review from '../src/component/Review';
import Add from '../src/component/Add';
import Setting from '../src/component/Setting';
import Support from '../src/component/Support';
import Stored from '../src/component/Stored';
import Login from '../src/component/Login';
import Signup from '../src/component/Signup'; 
import Loading from '../src/component/Loading';

import ProductsOverviewScreen from '../screens/shop/ProductsOverviewScreen';
import ProductDetailScreen from '../screens/shop/ProductDetailScreen';
import CartScreen from '../screens/shop/CartScreen';
import OrdersScreen from '../screens/shop/OrdersScreen';
import UserProductsScreen from '../screens/user/UserProductsScreen';
import EditProductScreen from '../screens/user/EditProductScreen';
import Colors from '../constants/Colors';
import BottomTabNavigator from '../constants/bottomTab';

const AppStack = createStackNavigator({

    Home: Homepage,
    //Chat: Chat,
    Map: Map,
   // Payment: Payment,
    //Profile: Profile,
    Review: Review,
    Add: Add,
    Setting: Setting,
    Support: Support,
    Stored: Stored,
    ProductsOverview: ProductsOverviewScreen,
    ProductDetail: ProductDetailScreen,
    Cart: CartScreen,
    

  
  });


  /////
  const AuthStack = createStackNavigator({
    Login: Login,
    Register: Signup,
  
  });

   const tttt= createSwitchNavigator(
      {
  
        // Loading: LoadingScreen,
        Loading: Loading,
        App: AppStack,
        Auth: AuthStack,
  
      },
      {
        initialRouteName: "Loading"
      },
      {
        defaultNavigationOptions: {
          headerStyle: {
            backgroundColor: Platform.OS === 'android' ? Colors.primary : ''
          },
          headerTitleStyle: {
            fontFamily: 'open-sans-bold'
          },
          headerBackTitleStyle: {
            fontFamily: 'open-sans'
          },
          headerTintColor: Platform.OS === 'android' ? 'white' : Colors.primary
        }
      }
    );


    ///////

    const defaultNavOptions = {
      headerStyle: {
        backgroundColor: Platform.OS === 'android' ? Colors.primary : ''
      },
      headerTitleStyle: {
        fontFamily: 'open-sans-bold'
      },
      headerBackTitleStyle: {
        fontFamily: 'open-sans'
      },
      headerTintColor: Platform.OS === 'android' ? 'white' : Colors.primary
    };
    
    const ProductsNavigator = createStackNavigator(
      {
        Chat: Chat,
        Home:Homepage,
        ProductsOverview: ProductsOverviewScreen,
        BottomTabNavigator,
        ProductDetail: ProductDetailScreen,
        Cart: CartScreen
      },
      {
        navigationOptions: {
          drawerIcon: drawerConfig => (
            <Ionicons
              name={Platform.OS === 'android' ? 'md-cart' : 'ios-cart'}
              size={23}
              color={drawerConfig.tintColor}
            />
          )
        },
        defaultNavigationOptions: defaultNavOptions
      }
    );
    
    const OrdersNavigator = createStackNavigator(
      {
        Orders: OrdersScreen
      },
      {
        navigationOptions: {
          drawerIcon: drawerConfig => (
            <Ionicons
              name={Platform.OS === 'android' ? 'md-list' : 'ios-list'}
              size={23}
              color={drawerConfig.tintColor}
            />
          )
        },
        defaultNavigationOptions: defaultNavOptions
      }
    );
    
    const AdminNavigator = createStackNavigator(
        {
          UserProducts: UserProductsScreen,
          EditProduct: EditProductScreen
        },
        {
          navigationOptions: {
            drawerIcon: drawerConfig => (
              <Ionicons
                name={Platform.OS === 'android' ? 'md-create' : 'ios-create'}
                size={23}
                color={drawerConfig.tintColor}
              />
            )
          },
          defaultNavigationOptions: defaultNavOptions
        }
      );
    
    const ShopNavigator = createDrawerNavigator(
      {
        BottomTabNavigator,
        Products: ProductsNavigator,
        Orders: OrdersNavigator,
        Admin: AdminNavigator,
        //StorageFavTabNavigator
      },
      {
        contentOptions: {
          activeTintColor: Colors.primary
        }
      }
    );


    
    
    export default createAppContainer(ShopNavigator);
    





    